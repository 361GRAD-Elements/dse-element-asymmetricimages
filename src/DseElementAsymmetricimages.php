<?php

/**
 * 361GRAD Element Asymmetricimages
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\ElementsBundle\ElementAsymmetricimages;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the 361GRAD Element Asymmetricimages.
 */
class DseElementAsymmetricimages extends Bundle
{
}
