<?php

/**
 * 361GRAD Element Asymmetricimages
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_asymmetricimages'] =
    '{type_legend},type,headline;' .
    '{firstimage_legend},dse_firstimage,dse_firstImageSize,dse_firstImageAlt;' .
    '{secondimage_legend},dse_secondimage,dse_secondImageSize,dse_secondImageAlt;' .
    '{customsettings_legend},dse_isMirror;' .
    '{margin_legend},dse_marginTop,dse_marginBottom;' .
    '{invisible_legend:hide},invisible,start,stop';

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstimage'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstimage'],
    'inputType' => 'fileTree',
    'eval'      => [
        'fieldType'  => 'radio',
        'filesOnly'  => true,
        'extensions' => Config::get('validImageTypes'),
        'tl_class'   => 'clr'
    ],
    'sql'       => 'binary(16) NULL'
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstImageSize'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['size'],
    'inputType' => 'imageSize',
    'options'   => System::getContainer()->get('contao.image.image_sizes')->getAllOptions(),
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval'      => [
        'rgxp'               => 'digit',
        'includeBlankOption' => true,
        'tl_class'           => 'w50',
    ],
    'sql'       => "varchar(64) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_firstImageAlt'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_firstImageAlt'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondimage'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondimage'],
    'inputType' => 'fileTree',
    'eval'      => [
        'fieldType'  => 'radio',
        'filesOnly'  => true,
        'extensions' => Config::get('validImageTypes'),
        'tl_class'   => 'clr'
    ],
    'sql'       => 'binary(16) NULL'
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondImageSize'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['size'],
    'inputType' => 'imageSize',
    'options'   => System::getContainer()->get('contao.image.image_sizes')->getAllOptions(),
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval'      => [
        'rgxp'               => 'digit',
        'includeBlankOption' => true,
        'tl_class'           => 'w50',
    ],
    'sql'       => "varchar(64) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondImageAlt'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondImageAlt'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_isMirror'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_isMirror'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w50 m12',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];