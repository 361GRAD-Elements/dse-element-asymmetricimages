<?php

/**
 * 361GRAD Element Asymmetricimages
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_asymmetricimages'] = ['Asymmetric Images', 'Asymmetric Images Teaser.'];

$GLOBALS['TL_LANG']['tl_content']['firstimage_legend']   = 'First Image Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_firstimage']   = ['First Image', 'Please select a file or folder from the files directory.'];
$GLOBALS['TL_LANG']['tl_content']['dse_firstImageSize']   = ['First Image Size', 'Here you can set the image dimensions and the resize mode.'];
$GLOBALS['TL_LANG']['tl_content']['dse_firstImageAlt']   = ['First Image Alt', 'You can enter an alternate text for the image (alt attribute) here.'];

$GLOBALS['TL_LANG']['tl_content']['secondimage_legend']   = 'Second Image Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_secondimage']   = ['Second Image', 'Please select a file or folder from the files directory.'];
$GLOBALS['TL_LANG']['tl_content']['dse_secondImageSize']   = ['Second Image Size', 'Here you can set the image dimensions and the resize mode.'];
$GLOBALS['TL_LANG']['tl_content']['dse_secondImageAlt']   = ['Second Image Alt', 'You can enter an alternate text for the image (alt attribute) here.'];

$GLOBALS['TL_LANG']['tl_content']['customsettings_legend']   = 'Custom Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_isMirror']   = ['Mirror Images', 'Check this if you want to mirror images'];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Margin Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Margin Top', 'Here you can add Margin to the top edge of the element (numbers only)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Margin Bottom', 'Here you can add Margin to the bottom edge of the element (numbers only)'];