<?php

/**
 * 361GRAD Element Asymmetricimages
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_asymmetricimages'] = ['Asymmetrische Bilder', 'Asymmetrische Bilder Teaser.'];

$GLOBALS['TL_LANG']['tl_content']['firstimage_legend']   = 'Erste Bildeinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_firstimage']   = ['Erstes Bild', 'Bitte wählen Sie eine Datei oder einen Ordner aus dem Dateiverzeichnis aus.'];
$GLOBALS['TL_LANG']['tl_content']['dse_firstImageSize']   = ['Erste Bildgröße', 'Hier können Sie die Bilddimensionen und den Größenänderungsmodus einstellen.'];
$GLOBALS['TL_LANG']['tl_content']['dse_firstImageAlt']   = ['Erstes Bild Alt', 'Hier können Sie einen alternativen Text für das Bild eingeben (altattribut).'];

$GLOBALS['TL_LANG']['tl_content']['secondimage_legend']   = 'Zweite Bildeinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_secondimage']   = ['Zweites Bild', 'Bitte wählen Sie eine Datei oder einen Ordner aus dem Dateiverzeichnis aus.'];
$GLOBALS['TL_LANG']['tl_content']['dse_secondImageSize']   = ['Zweite Bildgröße', 'Hier können Sie die Bilddimensionen und den Größenänderungsmodus einstellen.'];
$GLOBALS['TL_LANG']['tl_content']['dse_secondImageAlt']   = ['Zweites Bild Alt', 'Hier können Sie einen alternativen Text für das Bild eingeben (altattribut).'];

$GLOBALS['TL_LANG']['tl_content']['customsettings_legend']   = 'Benutzerdefinierte Einstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_isMirror']   = ['Spiegelbilder', ''];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Randeinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Rand oben', 'Hier können Sie Margin zum oberen Rand des Elements hinzufügen (nur nummern)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Rand unten', 'Hier können Sie dem unteren Rand des Elements Rand hinzufügen (nur nummern)'];